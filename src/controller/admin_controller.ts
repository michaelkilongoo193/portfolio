import { Request,NextFunction,Response } from "express";
import { AdminModel } from "../models/admin_model";

export class AdminController{

    static async getAdminIndex(req:Request,res:Response,next:NextFunction){
        res.render("a/index");
    }

    static async getProjectsList(req:Request,res:Response,next:NextFunction){
        const adminResult=await AdminModel.getAllProject().catch();

        res.render("a/project_list",{message:adminResult.message});
    }


    static async geEducationList(req:Request,res:Response,next:NextFunction){
        const adminResult=await AdminModel.getAllEducation().catch();

        res.render("a/education_list",{message:adminResult.message});
    }

    static async experienceList(req:Request,res:Response,next:NextFunction){
        const adminResult=await AdminModel.getAllExperience().catch();
        res.render("a/experience_list",{message:adminResult.message});
    }

    static async  inbox(req:Request,res:Response,next:NextFunction){
        res.render("a/table");
    }

}