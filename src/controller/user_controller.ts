import { NextFunction, Request,Response } from "express";
import { UserModel } from "../models/user_model";
import path = require("path");

export class UserController{

  static async index(req:Request,res:Response,next:NextFunction){
     res.render("index")
  }

  static async getContactPage(req:Request,res:Response,next:NextFunction){
   const sendEmail={
      status:false,
      message:"no thingto do"
   }
    res.render("contact",{data:sendEmail})
 }

 static async getContactRedirectPage(req:Request,res:Response,next:NextFunction){
  
    res.render("contact_redirect")
 }

 static async getResume(req:Request,res:Response,next:NextFunction){
   res.download(path.join(__dirname,"../upload/MICHAEL_CV.pdf"))
}


 static async postInfoDetail(req:Request,res:Response,next:NextFunction){
   const sendEmail=await UserModel.sendGmail(req.body).catch(next);
   res.render("contact",{data:sendEmail,})
}

 
 static async getServicePage(req:Request,res:Response,next:NextFunction){
  res.render("index")
}
static async getResumePage(req:Request,res:Response,next:NextFunction){
  const resumeResult= await UserModel.getResumes().catch(next);
  res.render("resume",{data: resumeResult.message})
}

static async getProjectPage(req:Request,res:Response,next:NextFunction){
   const projectResult= await UserModel.getAllProjects().catch(next);
   res.render("projects",{data:projectResult.message,})
 }

}