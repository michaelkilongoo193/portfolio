import { resolve } from 'path';
import { ProjectEntity, UserEntity } from '../entities';
import { ExpirenceEntity } from '../entities/experiance_entity';
import { LanguageEntity } from '../entities/language_entity';
import { EducationBackground } from './../entities/education_background';
import { Time } from '../util';
export class AdminModel{

    static async index(): Promise<any> {
        return new Promise(async (resolve, reject) => {
          try { 
          resolve({
              
            })     
          }
          catch (error) {
            console.log(error)
            reject(error) 
          }
        }); 
      }


      static async saveEducationBackGround(body:any): Promise<any> {
        return new Promise(async (resolve, reject) => {
          try { 
               const background=new EducationBackground({
                yeargape:body["start"],
                location:body["location"],
                college_name:body["college_name"],
                study:body["study"],
                user:await UserEntity.findOneBy({id:1})
               })
                await background.save();
                resolve({
                    status:true,
                    message:"saved successfully"
                  })     
          }
          catch (error) {
            console.log(error)
            reject(error) 
          }
        });
      
      }
 

      static async  getAllEducation(): Promise<any> {
        return new Promise(async (resolve, reject) => {
          try { 
            const education=await EducationBackground.find();
            let educations =education.map((e)=>{
              return{
                start:e.yeargape,
                location:e.location,
                college_name:e.college_name,
                study:e.study,}
            })
            resolve({
              status:true,
              message:educations
            })     
          }
          catch (error) {
            console.log(error)
            reject(error) 
          }
        });
      
      }



      static async saveExperiance(body:any): Promise<any> {
        return new Promise(async (resolve, reject) => {
          try { 
            const experience= new ExpirenceEntity({
                yeargape:body["start"],
                location:body["location"],
                position:body["position"],
                activities:body["activities"],
                user:await UserEntity.findOne({id:1})
            });  
            await experience.save();
          resolve({
              status:true,
              message:"saved successfully"
            })     
          }
          catch (error) {
            console.log(error)
            reject(error) 
          }
        });
      
      }


      static async getAllExperience() :Promise<any>{
        return new Promise(async (resolve,reject)=>{
          try{
            const experiences=await ExpirenceEntity.find();
            let experience =experiences.map((e)=>{
             return{
                 start:e.yeargape,
                 location:e.location,
                 position:e.position,
                 activities:e.activities,
                 createdDate:new Time(e.createdAt,"Africa/Dar_es_Salaam","DD-MM-YYYY").get(),
                 updated:new Time(e.updatedAt,"Africa/Dar_es_Salaam","DD-MM-YYYY").get()
               }
           });
           resolve({status:true,message:experience})


          }catch(error){reject(error)}



        })
      }

      static async saveLanguage(body:any): Promise<any> {
        return new Promise(async (resolve, reject) => {
          try { 
            const language= new LanguageEntity({
                name:body["name"],
                start_to_end:body["start_to_end"],
                user:await UserEntity.findOne({id:1})
            });  
            await language.save();
          resolve({
              status:true,
              message:"saved successfully"
            })     
          }
          catch (error) {
            console.log(error)
            reject(error) 
          }
        });
      
      }

      static async  getAllLanguage(): Promise<any> {
        return new Promise(async (resolve, reject) => {
          try { 
          const  language= await LanguageEntity.find();
            
          let languages =language.map((e)=>{
            return{
              name:e.name,
              start_to_end:e.start_to_end,
              createdDate:new Time(e.createdAt,"Africa/Dar_es_Salaam","DD-MM-YYYY").get(),
              updated:new Time(e.updatedAt,"Africa/Dar_es_Salaam","DD-MM-YYYY").get()
            }
          })
            resolve({
              status:true,
              message:languages
            })     
          }
          catch (error) {
            console.log(error)
            reject(error) 
          }
        });
      
      }




      static async saveProject(body:any): Promise<any> {
        return new Promise(async (resolve, reject) => {
          try { 
            const project= new ProjectEntity({
                name:body["name"],
                description:body["description"],
                github_link:body["github_link"],
                image:body["image"],
                user:await UserEntity.findOneBy({id:1})
            })
             await project.save();
          resolve({
              status:true,
              message:"saved successfully"
            })     
          }
          catch (error) {
            console.log(error)
            reject(error) 
          }
        });
      
      }

      static async  getAllProject(): Promise<any> {
        return new Promise(async (resolve, reject) => {
          try { 
            const project= await  ProjectEntity.find();
            let projects =project.map((e)=>{
              return{
                name:e.name,
                description:e.description,
                github_link:e.github_link,
                image:e.image}
            })
            resolve({
              status:true,
              message:projects
            })     
          }
          catch (error) {
            console.log(error)
            reject(error) 
          }
        });
      
      }



    
}