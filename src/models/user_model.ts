import { ProjectEntity } from "../entities";
import { EducationBackground } from "../entities/education_background";
import { ExpirenceEntity } from "../entities/experiance_entity";
import { LanguageEntity } from "../entities/language_entity";
import { Email, Time } from "../util";

export class UserModel{

    static async sendGmail(body:any): Promise<any> {
        return new Promise(async (resolve, reject) => {
          try { 
           const emailResult= await Email.send(body["email"],  body["subject"],  body["message"] );
          resolve({
              status:emailResult.status,
              message:emailResult.message
            })     
          }
          catch (error) {
            console.log(error)
            reject(error) 
          }
        });
      
      }
      static async getAllProjects(): Promise<any> {
        return new Promise(async (resolve, reject) => {
          try { 
             const projects= await ProjectEntity.find();
              let project_list=projects.map((e)=>{
                return {
                  name:e.name,
                  description:e.description,
                  github_link:e.github_link,
                  image:e.image,
                  createdDate:new Time(e.createdAt,"Africa/Dar_es_Salaam","DD-MM-YYYY").get(),
                  updated:new Time(e.updatedAt,"Africa/Dar_es_Salaam","DD-MM-YYYY").get()
                }
              })
          resolve({
              status:true,
              message:project_list
            })     
          }
          catch (error) {
            console.log(error)
            reject(error) 
          }
        });
      
      }


static async getResumes(): Promise<any> {
  return new Promise(async (resolve, reject) => {
    try { 
        Promise.all([
         await ExpirenceEntity.find(),
         await LanguageEntity.find(),
         await EducationBackground.find(),
         await ProjectEntity.find()
        ])
        .then(async ([experiance,language,education,project])=>{
          let experiences =experiance.map((e)=>{
            return{
                start:e.yeargape,
                location:e.location,
                position:e.position,
                activities:e.activities,
                createdDate:new Time(e.createdAt,"Africa/Dar_es_Salaam","DD-MM-YYYY").get(),
                updated:new Time(e.updatedAt,"Africa/Dar_es_Salaam","DD-MM-YYYY").get()
              }
          });

          let languages =language.map((e)=>{
            return{
              name:e.name,
              start_to_end:e.start_to_end,
              createdDate:new Time(e.createdAt,"Africa/Dar_es_Salaam","DD-MM-YYYY").get(),
              updated:new Time(e.updatedAt,"Africa/Dar_es_Salaam","DD-MM-YYYY").get()
            }
          })

          let educations =education.map((e)=>{
            return{
              start:e.yeargape,
              location:e.location,
              college_name:e.college_name,
              study:e.study,}
          })

          let projects =project.map((e)=>{
            return{
              name:e.name,
              description:e.description,
              github_link:e.github_link,
              image:e.image}
          })

          resolve({
            status:true,
            message:{education,language,experiance,projects}
          })
        })   
    }
    catch (error) {
      console.log(error)
      reject(error) 
    }
  });

}


      



}