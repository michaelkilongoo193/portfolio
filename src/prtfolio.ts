import * as express from "express"
import * as bodyParser from "body-parser"
import { Request, Response } from "express"
import { AppDataSource } from "./ormconfig"
import cors=require("cors")
const helmet = require("helmet");
const morgan = require("morgan");
const cookieParser=require("cookie-parser")
import {join} from "path"
import { userRouter } from "./routes";

import flash = require("connect-flash");
import { adminRoute } from "./routes/admin_route"
var session = require("express-session");
require("dotenv").config()

AppDataSource.initialize().then(async () => {

       // create express app
       const app = express()
       var whitelist = ['*']
       var corsOptions = {
           origin: function (origin:any, callback:any) {
               if (whitelist.indexOf(origin) !== -1) {
                   callback(null, true)
               } else {
                   callback(new Error('Not allowed by CORS'))
               }
           },
           methods: ['GET','POST','DELETE','UPDATE','PUT','PATCH']
       }
   
       app.use(
           session({
             resave: false,
             secret: "DOCTOR_SECRET",
             cookie: {
               // secure: true,
               maxAge: 60000,
             },
             saveUninitialized: true,
           })
         );
   
   
         app.use(
           express.static(join(__dirname, "public"), {
             etag: true,
             maxAge: 3600,
           })
         );
       app.use(express.static(join(__dirname, "views")));
       app.set("views", join(__dirname, "views"));
       app.set("view engine", "ejs");
   
       app.use(cors({origin:"*"}))
       app.use(helmet())
       app.use(morgan("dev"))
       app.use(cookieParser("DOCTOR_SECRET"));
       app.use(bodyParser.json())
       app.use(bodyParser.urlencoded({extended:true}))
       
   
       app.use("",userRouter)
       app.use("/a",adminRoute)

       

   
       app.listen(process.env.PORT,()=> {  
           console.log(`http://localhost:${process.env.PORT}`)
       })




}).catch(error => console.log(error))
