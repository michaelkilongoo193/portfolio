export function getTimeDifference(date:any){
   const today=new Date();
   const timediff=today.getTime()-date.getTime();
   const tohour=timediff/(1000*60)
   return Math.round(tohour)
}