import * as env from "dotenv";
import { Request } from "express";
import * as jwt from "jsonwebtoken";

env.config();
export class Auth {
  public id: number;
  public role: string;
  public token_id: number;
  public email: string;
  public islogin: boolean;

  constructor(req: Request) {
    try {
      const token = req.cookies.appointment || "";
      if (token) {
        const payload = jwt.verify(token, process.env.SECRET_KEY) as any;

        this.id = payload.id;
        this.role = payload.role;
        this.token_id = payload.token_id;
        this.email = payload.email;
        this.islogin = payload.islogin;

      }
    } catch (error) {
      console.log(error)
      throw new Error(error);
    }
  }
}
