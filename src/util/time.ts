import moment = require("moment-timezone");

export class Time {
  time: Date;
  zone: string;
  format: string;

  constructor(time: Date, zone: string, format: string) {
    this.time = time;
    this.zone = zone;
    this.format = format;
  }

  get(): any {
    return moment(this.time).tz(this.zone).format(this.format);
  }

  static timeCompare(createdAt: Date, updatedAt: Date): boolean {
    if (
      moment(createdAt).tz("Africa/Dar_es_Salaam").unix() !=
      moment(updatedAt).tz("Africa/Dar_es_Salaam").unix()
    ) {
      return true;
    } else {
      return false;
    }
  }
}
