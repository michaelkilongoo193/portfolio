import * as fs from "fs";
import path = require("path");

export class Fs {
  /** File check function handler */
  static fileCheck(): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        let uploadPath = path.join(__dirname, "../../upload");
        let imagePath = path.join(uploadPath, "/images");
        let videoPath = path.join(uploadPath, "/videos");
        let documentPath = path.join(uploadPath, "/documents");
        let reportPath = path.join(documentPath, '/report');

        if (!fs.existsSync(uploadPath)) {
          fs.mkdirSync(uploadPath);
          fs.mkdirSync(imagePath);
          fs.mkdirSync(videoPath);
          fs.mkdirSync(documentPath);
          fs.mkdirSync(reportPath);
        } else {
          if (!fs.existsSync(imagePath)) {
            fs.mkdirSync(imagePath);
          }
          if (!fs.existsSync(videoPath)) {
            fs.mkdirSync(videoPath);
          }
          if (!fs.existsSync(documentPath)) {
            fs.mkdirSync(documentPath);
          }
          if (!fs.existsSync(reportPath)) {
            fs.mkdirSync(reportPath);
          }
        }
        resolve("Folder created successful.");
      } catch (error) {
        reject(error);
      }

    })
  }
}