import path = require("path");
import multer = require("multer");

export class MediaUpload {
  static upload(fieldName: string): any {
    return multer({
      storage: multer.diskStorage({
        destination: (req: any, file: any, cb: any) => {
          let path_to =
            file.mimetype == "video/mp4"
              ? `../../upload/videos`
              : `../../upload/images`;
          cb(null, path.join(__dirname, path_to));
        },
        filename: (req: any, file: any, cb: any) => {
          cb(
            null,
            file.fieldname + "-" + Date.now() + path.extname(file.originalname)
          );
        },
      }),
      fileFilter: (req: any, file: any, cb: any) => {
        if (
          file.mimetype !== "video/mp4" &&
          file.mimetype !== "image/jpg" &&
          file.mimetype !== "image/jpeg" &&
          file.mimetype !== "image/png"
        ) {
          req.fileValidationError = "The file format is not supported.";
          cb(new Error("The file format is not supported"));
        }
        cb(null, true);
      },
    }).single(fieldName);
  }
}
