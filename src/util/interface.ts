export interface SendSmsInterface {
  message: string;
  recipients: any[];
}
