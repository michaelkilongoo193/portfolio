
import * as env from "dotenv";
import * as nodemailer from "nodemailer";

env.config();
export class Email {

  static async  send(to:string,subject:string,message:string): Promise<any> {
    return new Promise((resolve, reject) => {
      // create reusable transporter object using the default SMTP transport
      let transporter = nodemailer.createTransport({
        service: "gmail",
        host: "smtp.gmail.com",
        port: 587,
        secure: false,
        auth: {
          user: "michaelkilongoo193@gmail.com",
          pass: "sapna1620",
        },     
   

      });
  
      // send mail with defined transport object
      transporter.sendMail(
        {
          to: to, // receivers address
          subject: subject, // Subject line
          html: `${message}`, // html body
          from: process.env.EMAIL_SENDER, // sender address
        },
        function (error:any, info:any) {
          if(error) resolve({ 
            status: false, message: "Somethings went wrong try again later." });
          resolve({ status: true, message: "We have sent confirmation link to your email." });
        }
      );
    });
  }
}

interface EmailParameter {
  to: string,
  subject: string, message: string
}