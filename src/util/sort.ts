export class Sort {
  static data(body: any): Promise<any> {
    return new Promise((resolve, reject) => {
      try{
        let data = body.data;
        let property = body.property;
        data.sort((a: any, b: any) => {
          return a[`${property}`] - b[`${property}`]; });
        resolve(data);
      }catch(error){ reject(error); }
    })
  }
}