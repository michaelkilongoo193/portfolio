import * as env from "dotenv";
import * as jwt from "jsonwebtoken";
import { Response, Request, NextFunction } from "express";

env.config();
export class Token {
  static genarate(data: Genarate): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const token = jwt.sign(
          {
            id: data.id,
            role: data.role,
            token_id: data.token_id,
            email: data.email,
            islogin:data.islogin
          },
          process.env.SECRET_KEY,
          { expiresIn: "2h", algorithm: "HS256" }
        );
        resolve(token);
      } catch (error) {
        reject(error.toString());
      }
    });
  }

  /** */
  static async verify(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<any> {
    const token = req.cookies. appointment|| "";
    try {
      if (!token) {
        res.send({ status: 401, message: "You are not authorized." });
      } else {
        jwt.verify(token, process.env.SECRET_KEY);
        next();
      }
    } catch (error) {
      if (error["message"] == "invalid token") {
        res.send({ status: 401, message: error["message"] });
      } else {
        res.send({ status: 401, message: error });
      }
    }
  }
}

interface Genarate {
  id: number;
  role: string;
  token_id: number;
  email: string;
  islogin: boolean;
}
