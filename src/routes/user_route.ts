import { UserController } from "../controller";

const express=require("express")
export const userRouter = express.Router();
userRouter.get("/",UserController.index)
userRouter.get("/contact",UserController.getContactPage)
userRouter.post("/contact_us",UserController.postInfoDetail)
userRouter.get("/download_resume",UserController.getResume)
userRouter.get("/resume",UserController.getResumePage)
userRouter.get("/project",UserController.getProjectPage)
userRouter.get("/service",UserController.getServicePage)