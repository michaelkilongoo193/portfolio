import { AdminController } from "../controller";
const express=require("express")

export const adminRoute = express.Router();
adminRoute.get("/",AdminController.getAdminIndex);
adminRoute.get("/educationList",AdminController.geEducationList);
adminRoute.get("/projectList",AdminController.getProjectsList);
adminRoute.get("/experienceList",AdminController.experienceList);
adminRoute.get("/inbox",AdminController.inbox);





