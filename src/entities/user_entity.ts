import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany, CreateDateColumn, UpdateDateColumn, BeforeUpdate, BeforeInsert } from "typeorm"
import { ProjectEntity } from "./project_entity"
import { LanguageEntity } from "./language_entity";
import { ExpirenceEntity } from "./experiance_entity";
import { EducationBackground } from "./education_background";

@Entity({name:"user"})
export class UserEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    full_ame: string;

    @Column()
    email: string;

    @Column()
    instagram: string;

    @Column()
    twitter: string;

    @Column()
    linked_in: string;

    @Column()
    birth_date: string

    @Column()
    image: string

    @Column()
    about_me: string

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;
  
    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;
  
    @BeforeInsert()
    setCreatedAt() {
      this.createdAt = new Date();
    }
  
    @BeforeUpdate()
    setUpdatedAt() {
      this.updatedAt = new Date();
    }
    // end date

    @OneToMany((type) => ProjectEntity, (project) => project.user)
    projects: ProjectEntity[];


    @OneToMany((type) => LanguageEntity, (project) => project.user)
    languages: LanguageEntity[];

    @OneToMany((type) => EducationBackground, (educations) => educations.user)
    educations: EducationBackground[];

    @OneToMany((type) => ExpirenceEntity, (experiance) => experiance.user)
    experiance: ExpirenceEntity[];

    constructor(data:UserParameter){
        super();
       if(data){
                this.full_ame=data.full_ame;
                this.email=data.email;
                this.instagram=data.instagram;
                this.twitter=data.twitter;
                this.linked_in=data.linked_in;
                this.birth_date=data.birth_date;
                this.image=data.image;
       }
 
    }

}


interface UserParameter{
    full_ame: string;
    email: string;
    instagram: string;
    twitter: string;
    linked_in: string;
    birth_date: string;
    image: string;
}
