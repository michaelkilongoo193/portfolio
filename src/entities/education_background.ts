import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { UserEntity } from "./user_entity";

@Entity({name:"education"})
export class EducationBackground extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    yeargape:string;

    @Column()
    college_name:string;

    @Column()
    study:string;

    @Column()
    location:string;

    @ManyToOne((type) => UserEntity, (user) => user.educations, { nullable: false })
    user: UserEntity;
    
    constructor(data?:EducationParameter){
        super();
       if(data){
         this.yeargape=data.yeargape;
         this.college_name=data.college_name;
         this.study=data.study;
         this.location=data.location;
         this.user=data.user;
       }
 
    }
 
 
 
 
 }
 
 
 
 interface EducationParameter{
    yeargape:string;
    location:string;
    college_name:string;
    study:string;
    user:UserEntity;
 }