import { BaseEntity, BeforeInsert, BeforeUpdate, Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { UserEntity } from './user_entity';

@Entity({name:"language"})
export class LanguageEntity extends BaseEntity{

    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable:false,unique:true})
    name: string;

    @Column({})
    start_to_end: string;

    @ManyToOne((type) => UserEntity, (user) => user.languages, { nullable: false })
    user: UserEntity;


    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;
  
    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;
  
    @BeforeInsert()
    setCreatedAt() {
      this.createdAt = new Date();
    }
  
    @BeforeUpdate()
    setUpdatedAt() {
      this.updatedAt = new Date();
    }
    // end date

    constructor(data?:LanguageParameter){
        super();
       if(data){
         this.name=data.name;
         this.user=data.user;
         this.start_to_end=data.start_to_end;
       }
 
    }
 
 
 
 
 }
 
 
 
 interface LanguageParameter{
    name:string;
    start_to_end:string;
    user:UserEntity;
 }