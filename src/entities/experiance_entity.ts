import { BaseEntity, BeforeUpdate, Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { UserEntity } from "./user_entity";
import { BeforeInsert } from "typeorm";

@Entity({name:"experiance"})
export class ExpirenceEntity extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    yeargape:string;

    @Column()
    location:string;

    @Column()
    position:string;

    @Column()
    activities:string;

    @ManyToOne((type) => UserEntity, (user) => user.experiance, { nullable: false })
    user: UserEntity;


    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;
  
    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;
  
    @BeforeInsert()
    setCreatedAt() {
      this.createdAt = new Date();
    }
  
    @BeforeUpdate()
    setUpdatedAt() {
      this.updatedAt = new Date();
    }
    // end date

    constructor(data?:ExpirenceParameter){
        super();
       if(data){
         this.yeargape=data.yeargape;
         this.location=data.location;
         this.position=data.position;
         this.activities=data.activities;
         this.user=data.user;
       }
 
    }
 
 
 
 
 }
 
 
 
 interface ExpirenceParameter{
    yeargape:string;
    location:string;
    position:string;
    activities:string;
    user:UserEntity;
 }