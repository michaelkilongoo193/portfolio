import { BaseEntity, BeforeInsert, BeforeUpdate, Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { UserEntity } from "./user_entity";

@Entity({name:"project"})
export class ProjectEntity extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number
    
    @Column({nullable:false})
    name:string;

    @Column()
    description:string;

    @Column({unique:true,nullable:false})
    github_link:string;

    @Column()
    image:string;

    @ManyToOne((type) => UserEntity, (user) => user.projects, { nullable: false })
    user: UserEntity;

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;
  
    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;
  
    @BeforeInsert()
    setCreatedAt() {
      this.createdAt = new Date();
    }
  
    @BeforeUpdate()
    setUpdatedAt() {
      this.updatedAt = new Date();
    }
    // end date

    


   constructor(data?:ProjectParameter){
       super();
      if(data){
        this.name=data.name;
        this.description=data.description;
        this.github_link=data.github_link;
        this.user=data.user;
        this.image=data.image;
      }

   }




}



interface ProjectParameter{
   name:string;
   description:string;
   github_link:string;
   image?:string;
   user:UserEntity;


}