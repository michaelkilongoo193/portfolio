
import "reflect-metadata";
import { join } from "path";
import { DataSource } from "typeorm";

export const AppDataSource = new DataSource({
    type: "mysql",
    host: "127.0.0.1",
    port: 3306,
    username: "root",
    password: "12345",
    database: "test1",
    ssl:
      process.env.NODE_ENV === "production"
        ? { rejectUnauthorized: false }
        : false,
    entities: [join(__dirname, "./entities/*{.ts,.js}")],
    // We are using migrations, synchronize should be set to false.
    synchronize: true,
    dropSchema:false,
    // Run migrations automatically,
    // you can disable this if you prefer running migration manually.
    migrationsRun: true,
    logging: ["warn", "error"],
    logger: process.env.NODE_ENV === "production" ? "file" : "debug",
    migrations: [join(__dirname, "./migrations/*{.ts,.js}")],
})
